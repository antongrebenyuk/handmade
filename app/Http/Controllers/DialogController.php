<?php

namespace App\Http\Controllers;

use App\Models\Dialog;
use App\Models\Message;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Product;
use App\Models\Product_photo;
use App\Models\Product_age;
use App\Models\Category;
use App\Models\User;
use App\Http\Requests\DialogRequest;
use App\Models\Order;
use App\Models\Items;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class DialogController extends Controller
{
    public function showDialog($product_id, $seller_id, $buyer_id)
    {
        //если продавец или покупатель будет писать самому себе
        if ($seller_id == $buyer_id)
        {
            return view('dialogs.errorDialog', compact(['countProducts']));
        }
        if ($seller_id!=Auth::user()->id && $buyer_id!=Auth::user()->id)
        {
            return view('dialogs.errorDialog', compact(['countProducts']));
        }
        else
        {
            //узнаем есть ли такой диалог в базе
            $dialog = Dialog::
            where([
                ['product_id', '=', $product_id],
                ['buyer_id', '=', $buyer_id],
                ['seller_id', '=', $seller_id],
            ])->first();
            //достаем информацию об обсуждаемом товаре
            $products = Product::where('id', $product_id)->get();
            foreach ($products as $product)
            {
                $photo = Product_photo::where('product_id', '=', $product->id)->first();
                $product->photo_id = $photo->photo;
                $age = Product_age::where('id', '=', $product->product_ages_id)->first();
                $product->product_ages_id = $age->name;
                $category = Category::where('id', '=', $product->category_id)->first();
                $product->category_id = $category->name;
            }
            //получаем информацию о покупателе
            $users_buyer = User::where('id', $buyer_id)->get();
            foreach ($users_buyer as $user_buyer)
            {
                $test = $user_buyer->id;
                $photo = User::where('id', '=', $user_buyer->id)->first();
                $user_buyer->photo_id = $photo->photo;
                $buyer = $user_buyer;
            }
            //получаем информацию о продавце
            $users_seller = User::where('id', $seller_id)->get();
            foreach ($users_seller as $user_seller)
            {
                $photo = User::where('id', '=', $user_seller->id)->first();
                $user_seller->photo_id = $photo->photo;
                $seller = $user_seller;
            }
            if (!empty($dialog))
            {
                $messages = Message::where('dialog_id', '=', $dialog->id)->get();
                return view('dialogs.showDialog', compact(['products', 'messages', 'buyer', 'seller' , 'countProducts']));
            }
            else
            {
                //если диалога еще нет, то выводится только информация о товаре
                return view('dialogs.showDialog', compact(['products', 'buyer', 'seller' , 'countProducts']));
            }
        }
    }

    public function createDialogAndMessage(DialogRequest $request, $product_id, $seller_id, $buyer_id)
    {
        //достаем информацию об обсуждаемом товаре
        $products = Product::where('id', $product_id)->get();
        foreach ($products as $product)
        {
            $photo = Product_photo::where('product_id', '=', $product->id)->first();
            $product->photo_id = $photo->photo;
            $age = Product_age::where('id', '=', $product->product_ages_id)->first();
            $product->product_ages_id = $age->name;
            $category = Category::where('id', '=', $product->category_id)->first();
            $product->category_id = $category->name;
        }
        //получаем информацию о покупателе
        $users_buyer = User::where('id', $buyer_id)->get();
        foreach ($users_buyer as $user_buyer)
        {
            $photo = User::where('id', '=', $user_buyer->id)->first();
            $user_buyer->photo_id = $photo->photo;
            $buyer = $user_buyer;
        }
        //получаем информацию о продавце - тот, кто написал сообщение
        $users_seller = User::where('id', $seller_id)->get();
        foreach ($users_seller as $user_seller)
        {
            $photo = User::where('id', '=', $user_seller->id)->first();
            $user_seller->photo_id = $photo->photo;
            $seller = $user_seller;
        }
        //проверяем есть ли уже такой диалог
        $dialog = Dialog::where([
            ['buyer_id', '=', $buyer_id],
            ['seller_id', '=', $seller_id],//он же отправитель
            ['product_id', '=', $product_id],
        ])->first();
        //если такого диалога нет, то создаем его
        if (empty($dialog))
        {
            $addDialog = new Dialog();
            $addDialog->product_id = $product_id;
            $addDialog->buyer_id = $buyer_id;
            $addDialog->seller_id = $seller_id;
            $addDialog->save();
            //получаем ид созданного диалога
            $dialog_id = Dialog::where([
                ['buyer_id', '=', $buyer_id],
                ['seller_id', '=', $seller_id],
                ['product_id', '=', $product_id],
            ])->get('id');
            foreach ($dialog_id as $id_item) {
                $myDialogId = $id_item->id;
            }
            //сохраняем сообщение в новом созданном диалоге
            $addMessage = new Message();
            $addMessage->content = $request->message;
            $messagePhoto = $request->messagePhoto;
            if (!empty($messagePhoto))
            {
                $img = $request->file('messagePhoto');
                $extensions = $img->extension();
                $nameFile = 'message_photo_' . Date('Y-m-d_H-i-s') . 'user_' . Auth::id() . '.' . $extensions;
                $addMessage->photo = $nameFile;
                Storage::putFileAs('public/img', $request->messagePhoto, $nameFile);
            }
            $addMessage->dialog_id = $myDialogId;
            $addMessage->user_id = Auth::user()->id;
            $addMessage->save();
            //выбираем все сообщения которые относятся к этому диалогу
            $messages = Message::where('dialog_id', '=', $myDialogId)->get();
            return view('dialogs.showDialog', compact(['products', 'messages', 'buyer', 'seller' , 'countProducts']));
        }
        else //если диалог есть, то сохраняем сообщение и выводим сообщения по нему
        {
            $addMessage = new Message();
            $addMessage->content = $request->message;
            $messagePhoto = $request->messagePhoto;
            if (!empty($messagePhoto))
            {
                $img = $request->file('messagePhoto');
                $extensions = $img->extension();
                $nameFile = 'message_photo_' . Date('Y-m-d_H-i-s') . 'user_' . Auth::id() . '.' . $extensions;
                $addMessage->photo = $nameFile;
                Storage::putFileAs('public/img', $request->messagePhoto, $nameFile);
            }
            $addMessage->dialog_id = $dialog->id;
            $addMessage->user_id = Auth::user()->id;
            $addMessage->save();
            $messages = Message::where('dialog_id', '=', $dialog->id)->get();
            return view('dialogs.showDialog', compact(['products', 'buyer', 'seller', 'messages', 'countProducts']));
        }
    }

    public function showDialogs()
    {
        $dialogs = Dialog::with([
            'messages' => function($query) {
                $query->orderBy('id', 'desc')
                    ->with(['author'])
                    ->select(['id', 'user_id', 'content', 'dialog_id']);
            },
            'product' => function($query) {
                $query->select(['id', 'name']);
            },
            'buyer' => function($query) {
                $query->where('id', '!=', Auth::id());
            },
            'seller' => function($query) {
                $query->where('id', '!=', Auth::id());
            },
        ])
            ->where('seller_id', '=', Auth::id())
            ->orWhere('buyer_id', '=', Auth::id())
            ->orderBy('id', 'desc')
            ->get();
        return view('dialogs.showDialogs', compact(['dialogs']));
    }
}
