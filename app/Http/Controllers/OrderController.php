<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Items;
use Illuminate\Support\Facades\Auth;
use App\Models\Product;
use App\Models\Product_photo;
use Illuminate\Support\Facades\DB;


class OrderController extends Controller
{
    public function showItemsInBasket()
    {
//        $userOrderStatus = Order::where('user_id', '=', Auth::user()->id)->get();
//        foreach ($userOrderStatus as $orderStatus) {
//            $status = $orderStatus->status_id;
//        }
//        dd($status);
        $orderId = Order::where('status_id', '=', 1)->where('user_id', '=', Auth::user()->id)->get('id');
        foreach ($orderId as $id) {
            $myOrderId = $id->id;
        }
        $userOrderId = Order::where('user_id', '=', Auth::user()->id)->count();
        if (!empty($userOrderId) && !empty($myOrderId) && $userOrderId !== 0) {
            $items = DB::table('items')
                ->join('products', 'items.product_id', '=', 'products.id')
                ->join('product_photos', 'product_photos.product_id', '=', 'products.id')
                ->where('items.order_id', '=', $myOrderId)
                ->get();
            foreach ($items as $item) {
                $itemsInBasket[] = $item;
            }
            return view('basket.basket', compact(['itemsInBasket']));
        }


        return view('basket.basket', ['itemsInBasket' => false]);
    }

    public function addItemToOrder(Request $request) {

        $product_id = $request->input('product_id'); // получаем id продукта
        $addOrder = new Order;
        $userOrderId = Order::where('user_id', '=', Auth::user()->id)->count();
        $userOrderStatus = Order::where('user_id', '=', Auth::user()->id)->get();
        foreach ($userOrderStatus as $orderStatus) {
            $status = $orderStatus->status_id;
        }
        // проверка на наличие заказа в БД(если нету, то создаем, если есть, то просто добавляем в него товар)
        if ($userOrderId === 0 || $status != 1){
            $addOrder->user_id = Auth::user()->id;
            $addOrder->save();
            $orderId = Order::where('status_id', '=', 1)->where('user_id', '=', Auth::user()->id)->get('id');
            foreach ($orderId as $id) {
                $myOrderId = $id->id;
            }
            $addItem = new Items;
            $addItem->product_id = $product_id;
            $addItem->order_id = $myOrderId;
            $addItem->save();
        }else{
            $orderId = Order::where('status_id', '=', 1)
                ->where('user_id', '=', Auth::user()->id)
                ->get('id');
            foreach ($orderId as $id) {
                $myOrderId = $id->id;
            }
            $quantityItems = Items::where('product_id', '=', $product_id)->where('status_id', '=', 1)->count();
//            dd($quantityItems);
            if ($quantityItems === 0){
                $addItem = new Items;
                $addItem->product_id = $product_id;
                $addItem->order_id = $myOrderId;
                $addItem->save();
            }else{
                $affected = DB::update('update items set quantity_items = quantity_items+1 where product_id = ?', [$product_id]);
            }
        }

        $items = DB::table('items')
            ->join('products', 'items.product_id', '=', 'products.id')
            ->where('items.order_id', '=', $myOrderId)
            ->orderBy('items.id', 'desc')
            ->limit(1)
            ->get();
        foreach ($items as $item) {
            $itemsInBasket = $item;
        }

        $countProducts = Items::where('order_id', '=', $myOrderId)->count();

        return json_encode([
            'countProducts' => $countProducts,
            'item' => $itemsInBasket
        ]);
    }

    public function destroyItemInOrder($id)
    {
        Items::where('product_id', '=', $id)->delete();
        $orderId = Order::where('status_id', '=', 1)->where('user_id', '=', Auth::user()->id)->get('id');
        foreach ($orderId as $order) {
            $myOrderId = $order->id;
        }
        $countItemInOrder = Items::where('order_id', '=', $myOrderId)->count();
        if ($countItemInOrder === 0){
            Order::where('id', '=', $myOrderId)->delete();
        }


        return redirect(route('basket'));
    }

    public function getSum(Request $request)
    {
        $quantity = $request->input('quantity');
        $product_id = $request->input('product_id');

        $productPrice = Product::find($product_id);
        $sum = $productPrice->price * $quantity;

        $affected = DB::update('update items set quantity_items = ? where product_id = ?', [$quantity, $product_id]);

        return json_encode($sum);
    }

    public function registerOrder()
    {
        $orderId = Order::where('status_id', '=', 1)->where('user_id', '=', Auth::user()->id)->get('id');
        foreach ($orderId as $order) {
            $myOrderId = $order->id;
        }

        $updateStatusOrder = DB::update('update orders set status_id = 2 where user_id = ?', [Auth::user()->id]);
        $updateStatusItems = DB::update('update items set status_id = 2 where order_id = ?', [$myOrderId]);

        return redirect('/');
    }

    public function editItemStatus(Request $request, $orderId, $productId)
    {

        $affected = DB::update('update items set status_id = ? where product_id = ? and order_id = ?', [$request->input('orderStatus'), $productId, $orderId]);

        return redirect(route('itemSales', $productId));
    }
}
