<?php

namespace App\Http\Controllers;

use App\Models\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Product_photo;
use App\Models\Product_age;
use App\Models\Product;
use App\Models\Order;
use App\Models\Items;

class PersonalAreaController extends Controller
{
    public function index()
    {
        $products = Product::where('user_id', Auth::user()->id)->paginate(9);
        foreach ($products as $product) {
            $photo = Product_photo::where('product_id', '=', $product->id)->first();
            $product->photo_id = $photo->photo;
            $age = Product_age::where('id', '=', $product->product_ages_id)->first();
            $product->product_ages_id = $age->name;
        }

        return view('personal_area.index', compact(['products']));
    }

    public function showMySales()
    {

        $items = Items::with([
            'product' => function ($query) {
                $query->with(['productPhoto'])->where('user_id', '=', Auth::id());
            },
            'order' => function ($query) {
                $query->with(['user']);
            },
            'status'
        ])->where('status_id', '!=', '1')->get();

        return view('personal_area.my_sales', compact(['items']));
    }

    public function showMyPurchases()
    {
        $items = Items::with([
            'product' => function ($query) {
                $query->with(['productPhoto']);
            },
            'order' => function ($query) {
                $query->with(['user'])->where('user_id', '=', Auth::id());
            },
            'status'
        ])->where('status_id', '!=', '1')->get();

//        dd($items);
        return view('personal_area.my_purchases', compact(['items']));
    }

    public function showMySalesOneItem($id)
    {
        $items = Items::with([
            'product' => function ($query) {
                $query->with('category', 'productPhoto', 'productAge');
            },
            'order' => function ($query) {
                $query->with('user');
            },
            'status'
        ])->where('status_id', '!=', '1')
            ->where('product_id', '=', $id)
            ->orderBy('id', 'desc')
            ->limit(1)
            ->get();
//        dd($items);

        $statuses = Status::all();

        return view('personal_area.my_sales_show', compact(['items', 'statuses']));
    }


}
