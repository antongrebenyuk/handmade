<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Http\Requests\FilterRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Product;
use App\Models\Product_photo;
use App\Models\Product_age;
use App\Models\Category;
use App\Models\Items;
use App\Models\Order;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::paginate(9);
        foreach ($products as $product) {
            $photo = Product_photo::where('product_id', '=', $product->id)->first();
            $product->photo_id = $photo->photo;
        }
        $newProducts = Product::all()->sortByDesc('id')->take(3);
        foreach ($newProducts as $newProduct) {
            $photo = Product_photo::where('product_id', '=', $newProduct->id)->first();
            $newProduct->photo_id = $photo->photo;
        }

        $categories = Category::all();
        $productAges = Product_age::all();

        return view('products.index', compact(['products', 'newProducts', 'categories', 'productAges']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $productAges = Product_age::all();

        return view('products.create', compact(['categories', 'productAges']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $addProduct = new Product;
        $product = $request->only(['name', 'price', 'color', 'quantity', 'age', 'category', 'description']);
        $addProduct->category_id = $product['category'];
        $addProduct->user_id = Auth::user()->id;
        $addProduct->name = $product['name'];
        $addProduct->price = $product['price'];
        $addProduct->description = $product['description'];
        $addProduct->color = $product['color'];
        $addProduct->product_ages_id = $product['age'];
        $addProduct->quantity = $product['quantity'];
        $addProduct->save();

        $products = Product::orderBy('id', 'desc')->limit(1)->get('id');
        foreach ($products as $product) {
            $lastProductId = $product->id;
        }
        $photo = new Product_photo;
        $img = $request->file('photo');
        $extensions = $img->extension();
        $nameFile = 'product_photo_main_' . Date('Y-m-d_H-i-s') . 'user_' . Auth::user()->id . '.' . $extensions;
        $photo->product_id = $lastProductId;
        $photo->photo = $nameFile;
        $photo->save();
        Storage::putFileAs('public/img', $request->photo, $nameFile);

        if ($request->file('photo2'))
        {
            $photo2 = new Product_photo;
            $img2 = $request->file('photo2');
            $extensions2 = $img2->extension();
            $nameFile2 = 'product_photo_2_' . Date('Y-m-d_H-i-s') . 'user_' . Auth::user()->id . '.' . $extensions2;
            $photo2->product_id = $lastProductId;
            $photo2->photo = $nameFile2;
            $photo2->save();
            Storage::putFileAs('public/img', $request->photo2, $nameFile2);
        }

        if ($request->file('photo3'))
        {
            $photo3 = new Product_photo;
            $img3 = $request->file('photo3');
            $extensions3 = $img3->extension();
            $nameFile3 = 'product_photo_3_' . Date('Y-m-d_H-i-s') . 'user_' . Auth::user()->id . '.' . $extensions3;
            $photo3->product_id = $lastProductId;
            $photo3->photo = $nameFile3;
            $photo3->save();
            Storage::putFileAs('public/img', $request->photo3, $nameFile3);
        }

        return redirect(route('allUserProducts'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $products = Product::where('id', $id)->get();
        foreach ($products as $product) {
            $photo = Product_photo::where('product_id', '=', $product->id)->get();
            $product->main_photo = $photo[0]->photo;
            if (isset($photo[1]))
            {
                $product->second_photo = $photo[1]->photo;
            }
            if (isset($photo[2]))
            {
                $product->third_photo = $photo[2]->photo;
            }
            $age = Product_age::where('id', '=', $product->product_ages_id)->first();
            $product->product_ages_id = $age->name;
            $category = Category::where('id', '=', $product->category_id)->first();
            $product->category_id = $category->name;
        }

        $allProducts = Product::find($id);

        return view('products.show', compact(['products', 'allProducts']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::all();
        $productAges = Product_age::all();
        $product = Product::find($id);
        if ($product->user_id == Auth::id()) {
            return view('products.edit', compact(['product', 'categories', 'productAges']));
        }

        return redirect('/');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductUpdateRequest $request, $id)
    {
        $product = $request->only(['name', 'price', 'color', 'quantity', 'age', 'category', 'description']);
        $updateProduct = Product::find($id);
        $updateProduct->category_id = $product['category'];
        $updateProduct->user_id = Auth::user()->id;
        $updateProduct->name = $product['name'];
        $updateProduct->price = $product['price'];
        $updateProduct->description = $product['description'];
        $updateProduct->color = $product['color'];
        $updateProduct->product_ages_id = $product['age'];
        $updateProduct->quantity = $product['quantity'];
        $updateProduct->save();

        if ($request->photo) {
            $photo = Product_photo::where('product_id', '=', $id)->first();
            $img = $request->file('photo');
            $extensions = $img->extension();
            $nameFile = 'product_photo_main_' . Date('Y-m-d_H-i-s') . 'user_' . Auth::user()->id . '.' . $extensions;
            $photo->photo = $nameFile;
            $photo->save();

            Storage::putFileAs('public/img', $request->photo, $nameFile);
        }





        return redirect(route('allUserProducts'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::find($id)->delete();

        return redirect(route('allUserProducts'));
    }

    /**
     * Фильтр продуктов
     *
     * @param Request $request
     */

    public function filterProducts(FilterRequest $request)
    {
        $categoryId = $request->input('categoryList');
        $ageProductId = $request->input('ageList');
        $minPrice = $request->input('minPrice');
        $maxPrice = $request->input('maxPrice');
        $color = $request->input('color');

        if(!$categoryId && !$ageProductId && !$minPrice && !$maxPrice && !$color){
            return redirect('/');
        }else{
            if ($request->input('maxPrice') && $request->input('minPrice') && $request->input('categoryList') && $request->input('ageList') && $request->input('color')) {
                $products = Product::where('price', '<=', $maxPrice)->where('color', 'like', '%' . $color . '%')->where('price', '>=', $minPrice)->where('category_id', '=', $categoryId)->where('product_ages_id', '=', $ageProductId)->paginate(9);
                // если указаны min и max цены, категория, возраст изделия и цвет
            } elseif ($request->input('maxPrice') && $request->input('minPrice') && $request->input('categoryList') && $request->input('ageList')) {
                $products = Product::where('price', '<=', $maxPrice)->where('price', '>=', $minPrice)->where('category_id', '=', $categoryId)->where('product_ages_id', '=', $ageProductId)->paginate(9);
                // если указаны min и max цены, категория и возраст изделия
            } elseif ($request->input('maxPrice') && $request->input('minPrice') && $request->input('categoryList')) {
                $products = Product::where('price', '<=', $maxPrice)->where('price', '>=', $minPrice)->where('category_id', '=', $categoryId)->paginate(9);
                // если указаны min и max цены и категория
            } elseif ($request->input('color') && $request->input('maxPrice') && $request->input('minPrice') && $request->input('categoryList')) {
                $products = Product::where('color', 'like', '%' . $color . '%')->where('price', '<=', $maxPrice)->where('price', '>=', $minPrice)->where('category_id', '=', $categoryId)->paginate(9);
                // если указаны min и max цены, категория и цвет
            } elseif ($request->input('maxPrice') && $request->input('minPrice') && $request->input('ageList')) {
                $products = Product::where('price', '<=', $maxPrice)->where('price', '>=', $minPrice)->where('product_ages_id', '=', $ageProductId)->paginate(9);
                // если указаны min и max цены и возраст изделия
            } elseif ($request->input('minPrice') && $request->input('ageList') && $request->input('categoryList')) {
                $products = where('price', '>=', $minPrice)->where('category_id', '=', $categoryId)->where('product_ages_id', '=', $ageProductId)->paginate(9);
                // если указаны min цена, категория и возраст
            } elseif ($request->input('maxPrice') && $request->input('ageList') && $request->input('categoryList')) {
                $products = where('price', '>=', $maxPrice)->where('category_id', '=', $categoryId)->where('product_ages_id', '=', $ageProductId)->paginate(9);
                // если указаны max цена, категория и возраст
            } elseif ($request->input('categoryList') && $request->input('ageList')) {
                $products = Product::where('category_id', '=', $categoryId)->where('product_ages_id', '=', $ageProductId)->paginate(9);
                // если указа категория и возраст изделия
            } elseif ($request->input('ageList') && $request->input('minPrice')) {
                $products = Product::where('product_ages_id', '=', $ageProductId)->where('price', '>=', $minPrice)->paginate(9);
                // если указан возраст изделия и min цена
            } elseif ($request->input('ageList') && $request->input('maxPrice')) {
                $products = Product::where('product_ages_id', '=', $ageProductId)->where('price', '>=', $maxPrice)->paginate(9);
                // если указан возраст изделия и max цена
            } elseif ($request->input('maxPrice') && $request->input('minPrice')) {
                $products = Product::where('price', '<=', $maxPrice)->where('price', '>=', $minPrice)->paginate(9);
                // если указана max и min цены
            } elseif ($request->input('categoryList') && $request->input('minPrice')) {
                $products = Product::where('category_id', '=', $categoryId)->where('price', '>=', $minPrice)->paginate(9);
                // если указана категория и min цена
            } elseif ($request->input('categoryList') && $request->input('minPrice')) {
                $products = Product::where('category_id', '=', $categoryId)->where('price', '>=', $minPrice)->paginate(9);
                // если указана категория и min цена
            } elseif ($request->input('categoryList') && $request->input('maxPrice')) {
                $products = Product::where('category_id', '=', $categoryId)->where('price', '<=', $maxPrice)->paginate(9);
                // если указана категория и max цена
            } elseif ($request->input('categoryList') && $request->input('color')) {
                $products = Product::where('color', 'like', '%' . $color . '%')->where('category_id', '=', $categoryId)->paginate(9);
                // если указан возраст изделия и цвет
            } elseif ($request->input('categoryList')) {
                $products = Product::where('category_id', '=', $categoryId)->paginate(9);
                // если указана категория
            } elseif ($request->input('color') && $request->input('minPrice')) {
                $products = Product::where('price', '>=', $minPrice)->where('color', 'like', '%' . $color . '%')->paginate(9);
                // мин цена и цвет
            } elseif ($request->input('minPrice')) {
                $products = Product::where('price', '>=', $minPrice)->paginate(9);
                // если указана min цена
            } elseif ($request->input('color') && $request->input('maxPrice')) {
                $products = Product::where('price', '<=', $maxPrice)->where('color', 'like', '%' . $color . '%')->paginate(9);
                // макс цена и цвет
            } elseif ($request->input('maxPrice')) {
                $products = Product::where('price', '<=', $maxPrice)->paginate(9);
                // если указана max цена
            } elseif ($request->input('ageList') && $request->input('color')) {
                $products = Product::where('color', 'like', '%' . $color . '%')->where('product_ages_id', '=', $ageProductId)->paginate(9);
                // если указан возраст изделия и цвет
            } elseif ($request->input('ageList')) {
                $products = Product::where('product_ages_id', '=', $ageProductId)->paginate(9);
                // если указан возраст изделия
            } elseif ($request->input('color')) {
                $products = Product::where('color', 'like', '%' . $color . '%')->paginate(9);
            }


            foreach ($products as $product) {
                $photo = Product_photo::where('product_id', '=', $product->id)->first();
                $product->photo_id = $photo->photo;
            }
            $newProducts = Product::all()->sortByDesc('id')->take(3);
            foreach ($newProducts as $newProduct) {
                $photo = Product_photo::where('product_id', '=', $newProduct->id)->first();
                $newProduct->photo_id = $photo->photo;
            }

            $categories = Category::all();
            $productAges = Product_age::all();

            $category = Category::find($categoryId);
            $age = Product_age::find($ageProductId);

            return view('products.index', compact(['products', 'newProducts', 'categories', 'productAges',
                'categoryId', 'ageProductId', 'minPrice', 'maxPrice', 'category', 'ageProductId', 'age', 'color']));
        }

    }

    public function searchProduct(Request $request)
    {
        $searchProductName = $request->input('search');
        $products = Product::where('name', 'like', '%' . $searchProductName . '%')->paginate(9);

        foreach ($products as $product) {
            $photo = Product_photo::where('product_id', '=', $product->id)->first();
            $product->photo_id = $photo->photo;
        }
        $newProducts = Product::all()->sortByDesc('id')->take(3);
        foreach ($newProducts as $newProduct) {
            $photo = Product_photo::where('product_id', '=', $newProduct->id)->first();
            $newProduct->photo_id = $photo->photo;
        }

        $categories = Category::all();
        $productAges = Product_age::all();


        return view('products.index', compact(['products', 'newProducts', 'categories', 'productAges', 'searchProductName']));
    }

    public function showCategory($id)
    {
        $products = Product::where('category_id', '=', $id)->paginate(9);

        foreach ($products as $product) {
            $photo = Product_photo::where('product_id', '=', $product->id)->first();
            $product->photo_id = $photo->photo;
        }
        $newProducts = Product::all()->sortByDesc('id')->take(3);
        foreach ($newProducts as $newProduct) {
            $photo = Product_photo::where('product_id', '=', $newProduct->id)->first();
            $newProduct->photo_id = $photo->photo;
        }

        $categories = Category::all();
        $productAges = Product_age::all();

        return view('products.index', compact(['products', 'newProducts', 'categories', 'productAges']));
    }

    public function showItemsInBasket()
    {
        $orderId = Order::where('status_id', '=', 1)->where('user_id', '=', Auth::user()->id)->get('id');
        foreach ($orderId as $id) {
            $myOrderId = $id->id;
        }
        $items = DB::table('items')
            ->join('products', 'items.product_id', '=', 'products.id')
            ->join('product_photos', 'product_photos.product_id', '=', 'products.id')
            ->get();
        foreach ($items as $item) {
            $itemsInBasket[] = $item;
        }
//        foreach ($items as $item){
//            $itemsInBasket[] = Product::where('id', '=', $item->product_id)->get();
//            $photoProducts = Product_photo::where('product_id', '=', $item->product_id)->get();
//        }

        return view('products.index', compact(['itemsInBasket']));
//        return view('test', compact(['itemsInBasket']));
    }

}
