<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=> ['required', 'min:3', 'max:255'],
            'price' => ['required', 'numeric', 'between:1,999999'],
            'description' => ['required', 'min:5', 'max:1000'],
            'color' => ['required', 'min:3', 'max:255'],
            'photo' => ['required', 'max:1024', 'image'],
            'photo2' => ['nullable', 'max:1024', 'image'],
            'photo3' => ['nullable', 'max:1024', 'image'],
            'quantity' => ['required', 'numeric', 'min:1'],
            'age' => ['required'],
            'category' => ['required']
        ];
    }
}
