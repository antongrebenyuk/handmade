<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function dialog()
    {
        return $this->hasMany('App\Models\Dialog');

//    public function getInfoMessage()
//    {
//        return $this->hasMany('App\Models\Message', 'dialog_id');
//    }
    }

    public function productPhoto()
    {
        return $this->belongsTo(Product_photo::class, 'id', 'product_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');

    }

    public function productAge()
    {
        return $this->belongsTo(Product_age::class, 'product_ages_id', 'id');

    }

}
