<?php


namespace App\Services;
use App\Models\Order;
use App\Models\Items;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class Basket
{
    public function getCountItemsInOrder()
    {
        $countProducts = 0;
        if (Auth::user()){
            $userOrderStatus = Order::where('user_id', '=', Auth::user()->id)->get();
            foreach ($userOrderStatus as $orderStatus) {
                $status = $orderStatus->status_id;
            }
            $addOrder = new Order;
            $userOrderId = Order::where('user_id', '=', Auth::user()->id)->count();
            if ($userOrderId !== 0){
                if (Auth::user() && $userOrderId){
                    $orderId = Order::where('status_id', '=', 1)->where('user_id', '=', Auth::user()->id)->get('id');
                    foreach ($orderId as $id) {
                        $myOrderId = $id->id;
                    }
                    if ($status === 1){
                        $countProducts = Items::where('order_id', '=', $myOrderId)->count();
                    }else{
                        $countProducts = 0;
                    }
                }else {
                    $countProducts = 0;
                }
            }
        }
        return $countProducts;
    }

    public function showItemsInBasket()
    {
        $orderId = Order::where('status_id', '=', 1)->where('user_id', '=', Auth::user()->id)->get('id');
        foreach ($orderId as $id) {
            $myOrderId = $id->id;
        }
        $items = DB::table('items')
            ->join('products', 'items.product_id', '=', 'products.id')
            ->where('items.order_id', '=', $myOrderId)
            ->orderBy('items.id', 'desc')
            ->limit(1)
            ->get();
        foreach ($items as $item) {
            $itemsInBasket[] = $item;
        }
//        foreach ($items as $item){
//            $itemsInBasket[] = Product::where('id', '=', $item->product_id)->get();
//            $photoProducts = Product_photo::where('product_id', '=', $item->product_id)->get();
//        }


        return $itemsInBasket;
    }
}
