@extends('layouts.auth')

@section('content')
    <div class="container login">
        <div class="login__header">Авторизация</div>
        <div class="login__form__block">
            <form method="POST" class="login__form" action="{{ route('login') }}">
                @csrf
                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                    <div class="col-md-6">
                        <div class="error">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <input id="email" type="email" class="{{ $errors->has('email') ? 'form__error' : '' }}" name="email" value="{{ old('email') }}" autofocus>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Пароль') }}</label>

                    <div class="col-md-6">
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                        <input id="password" type="password" class="{{ $errors->has('password') ? ' form__error' : '' }}" name="password">
                    </div>
                </div>

                {{--<div class="form-group row">--}}
                {{--<div class="col-md-6 offset-md-4">--}}
                <div class="form-check">
                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                <label class="form-check-label" for="remember">
                {{ __('Запомнить меня') }}
                </label>
                </div>
                {{--</div>--}}
                {{--</div>--}}

                <div class="form-group row mb-0">
                    <div class="col-md-8 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Login') }}
                        </button>

                        {{--@if (Route::has('password.request'))--}}
                        {{--<a class="btn btn-link" href="{{ route('password.request') }}">--}}
                        {{--{{ __('Forgot Your Password?') }}--}}
                        {{--</a>--}}
                        {{--@endif--}}
                    </div>
                </div>
                <div class="no__account">Нет аккаунта? <a href="{{ route('register') }}">Регистрация</a></div>
            </form>
        </div>
    </div>
@endsection
