@extends('layouts.auth')

@section('content')
<div class="container register">
    <div class="register__header">Регистрация</div>
    <div class="register__form__block">
        <div class="requared__text"><span class="text-danger">'*'</span> - обязательные поля для заполнения</div>
        <form method="POST" action="{{ route('register') }}">
            @csrf
            <div class="form-group row">
                <label for="login" class="col-md-4 col-form-label text-md-right">{{ __('Логин') }}</label>
                <span class="text-danger">*</span>

                <div class="col-md-6">
                    @if ($errors->has('login'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('login') }}</strong>
                        </span>
                    @endif
                    <input id="login" type="text" class="{{ $errors->has('login') ? 'form__error' : '' }}" name="login" value="{{ old('login') }}">
                </div>
            </div>

            <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>
                <span class="text-danger">*</span>
                <div class="col-md-6">
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                    <input id="email" type="text" class="{{ $errors->has('email') ? 'form__error' : '' }}" name="email" value="{{ old('email') }}">
                </div>
            </div>

            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Пароль') }}</label>
                <span class="text-danger">*</span>
                <div class="col-md-6">
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                    <input id="password" type="password" class="{{ $errors->has('password') ? 'form__error' : '' }}" name="password">
                </div>
            </div>

            <div class="form-group row">
                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Повторите пароль') }}</label>
                <span class="text-danger">*</span>
                <div class="col-md-6">
                    @if ($errors->has('password_confirmation'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                    <input id="password-confirm" type="password" class="{{ $errors->has('password_confirmation') ? 'form__error' : '' }}l" name="password_confirmation">
                </div>
            </div>
            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Имя') }}</label>
                <span class="text-danger">*</span>
                <div class="col-md-6">
                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                    <input id="name" type="text" class="{{ $errors->has('name') ? 'form__error' : '' }}" name="name" value="{{ old('name') }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="surname" class="col-md-4 col-form-label text-md-right">{{ __('Фамилия') }}</label>
                <span class="text-danger">*</span>
                <div class="col-md-6">
                    @if ($errors->has('surname'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('surname') }}</strong>
                        </span>
                    @endif
                    <input id="surname" type="text" class="{{ $errors->has('surname') ? 'form__error' : '' }}" name="surname" value="{{ old('surname') }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="patronymic" class="col-md-4 col-form-label text-md-right">{{ __('Отчество') }}</label>
                <span class="text-danger">*</span>
                <div class="col-md-6">
                    @if ($errors->has('patronymic'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('patronymic') }}</strong>
                        </span>
                    @endif
                    <input id="patronymic" type="text" class="{{ $errors->has('patronymic') ? 'form__error' : '' }}" name="patronymic" value="{{ old('patronymic') }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Телефон') }}</label>
                <span class="text-danger">*</span>
                <div class="col-md-6">
                    @if ($errors->has('phone'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                    @endif
                    <input id="phone" type="text" class="{{ $errors->has('phone') ? 'form__error' : '' }}" name="phone" value="{{ old('phone') }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="birthday" class="col-md-4 col-form-label text-md-right">{{ __('Дата рождения') }}</label>
                <span class="text-danger">*</span>
                <div class="col-md-6">
                    @if ($errors->has('birthday'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('birthday') }}</strong>
                        </span>
                    @endif
                    <input id="birthday" min="1900-01-01" max="2100-01-01" type="date" class="{{ $errors->has('birthday') ? 'form__error' : '' }}" name="birthday" value="{{ old('birthday') }}">
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Регистрация') }}
                </div>
            </div>
        </form>
    </div>
</div>


<script>
    $(function(){
        $("#phone").mask("+38(099) 999-99-99");
    });
</script>
@endsection
