@extends('layouts.basket')

@section('content')
    <div class="container basket">
        <div class="navigation">
            <a href="/">HANDMADE.ярмарка</a>
            <span>►</span>
            <span>Корзина</span>
        </div>
        @if($itemsInBasket)
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Изделие</th>
                    <th scope="col">Наименование</th>
                    <th scope="col">Цена</th>
                    <th scope="col">Количество</th>
                    <th scope="col">Удалить</th>
                </tr>
                </thead>
                <tbody>
                @foreach($itemsInBasket as $itemInBasket)
                    <tr>
                        <td scope="row"><img src="/storage/img/{{ $itemInBasket->photo }}" alt=""></td>
                        <td class="name__pr__b"><a href="{{ route('showProduct', $itemInBasket->product_id) }}"
                               class="name__prouct__basket">{{ $itemInBasket->name }}</a></td>
                        <td>{{ $itemInBasket->price }}</td>
                        <td><input type="number" min="1" max="999" value="{{ $itemInBasket->quantity_items }}"
                                   data-item="{{ $itemInBasket->product_id }}" class="quantity__items"></td>
                        <td><a href="{{ route('destroyItem', $itemInBasket->product_id) }}"
                               onClick="return window.confirm('Удалить товар из корзины?');"><i
                                        class="far fa-trash-alt"></i></a></td>
                        @php
                            $sum = $itemInBasket->price * $itemInBasket->quantity_items;
                            $price[] = $sum;
                        @endphp
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="sum">Итого = {{ array_sum($price) }}</div>
            <div class="registration__order">
                <form action="{{ route('registerOrder') }}" method="post">
                    @csrf
                    <button type="submit" class="register__order__btn"
                            onClick="return window.confirm('Оформить заказ на сумму {{ array_sum($price).'грн' }}?');">
                        Оформить заказ
                    </button>
                </form>
            </div>
        @else
            <div>Корзина пуста</div>
        @endif
    </div>

    <script>
        $(function () {
            $('.quantity__items').change(function () {
                var quantity = $(this).val();
                var product_id = $(this).data("item");


                $.ajax({
                    url: '/get-sum',
                    type: 'POST',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: ({quantity: quantity, product_id: product_id}),
                    dataType: 'json',
                    success: function (sum) {

                    },
                    error: function () {

                    }
                })

            })
        })
    </script>
@endsection
