@extends('layouts.personalarea')

@section('content')

    <div class="container show__dialog">
        <div class="dialog__title">
            <div class="dialog__img">
                @foreach($products as $product)
                    <img src="/storage/img/{{ $product->photo_id }}" alt="" style="width: auto">
                @endforeach
            </div>
            <div class="dialog__title__info">
                <div class="dialog__info">
                    <div class="title">
                        @foreach($products as $product)
                            {{ $product->name }}
                        @endforeach
                    </div>
                    <div class="product__price">
                        {{ $product->price }} грн.
                    </div>
                </div>
            </div>
        </div>
    @if(!empty($messages))
            @foreach($messages as $message)
                {{--если отправитель сообщения я (правая сторона)--}}
                @if($message->user_id == Auth::user()->id)
                    {{--если я покупатель (берется информация из $buyer)--}}
                    @if($buyer->id == Auth::user()->id)
                        <div class="block_message">
                            <div class="block_message_info">
                                <div class="block_message_info_top">
                                    <div class="block_message_info_top_date_left">
                                        {{ date("d.m.Y H:m:s" ,strtotime($message->created_at)) }}
                                    </div>
                                    <div class="block_message_info_top_author_right">
                                        {{ $buyer->name }} {{ $buyer->surname }}
                                    </div>
                                </div>
                                <div class="block_message_info_bottom">
                                    {{ $message->content }}
                                </div>
                            </div>
                            <div class="block_message_img">
                                <img src="/storage/avatars/{{ $buyer->avatar }}">
                            </div>
                        </div>
                        @if(!empty($message->photo))
                            <div class="dialog_added_photo">
                                Прикрепленное фото:
                                <a href="/storage/img/{{ $message->photo }}" target="_blank"> <img src="/storage/img/{{ $message->photo }}" alt=""> </a>
                            </div>
                        @endif
                        {{--если я продавец (берется информация из $seller)--}}
                    @elseif($seller->id == Auth::user()->id)
                        <div class="block_message">
                            <div class="block_message_info">
                                <div class="block_message_info_top">
                                    <div class="block_message_info_top_date_left">
                                        {{ date("d.m.Y H:m:s" ,strtotime($message->created_at)) }}
                                    </div>
                                    <div class="block_message_info_top_author_right">
                                        {{ $seller->name }} {{ $seller->surname }}
                                    </div>
                                </div>
                                <div class="block_message_info_bottom">
                                    {{ $message->content }}
                                </div>
                            </div>
                            <div class="block_message_img">
                                <img src="/storage/avatars/{{ $seller->avatar }}">
                            </div>
                        </div>
                        @if(!empty($message->photo))
                            <div class="dialog_added_photo">
                                Прикрепленное фото:
                                <a href="/storage/img/{{ $message->photo }}" target="_blank"> <img src="/storage/img/{{ $message->photo }}" alt=""> </a>
                            </div>
                        @endif
                    @endif
                @endif
                {{--если отправитель сообщения не я (левая сторона)--}}
                @if($message->user_id != Auth::user()->id)
                    {{--если этот человек покупатель (берется информация из $buyer)--}}
                    @if($buyer->id == $message->user_id)
                        <div class="block_message">
                            <div class="block_message_img">
                                <img src="/storage/avatars/{{ $buyer->avatar }}">
                            </div>
                            <div class="block_message_info">
                                <div class="block_message_info_top">
                                    <div class="block_message_info_top_author_left">
                                        {{ $buyer->name }} {{ $buyer->surname }}
                                    </div>
                                    <div class="block_message_info_top_date_right">
                                        {{ date("d.m.Y H:m:s" ,strtotime($message->created_at)) }}
                                    </div>
                                </div>
                                <div class="block_message_info_bottom">
                                    {{ $message->content }}
                                </div>
                            </div>
                        </div>
                        @if(!empty($message->photo))
                            <div class="dialog_added_photo">
                                Прикрепленное фото:
                                <a href="/storage/img/{{ $message->photo }}" target="_blank"> <img src="/storage/img/{{ $message->photo }}" alt=""> </a>
                            </div>
                        @endif
                        {{--если этот человек продавец (берется информация из $seller)--}}
                    @elseif($seller->id == $message->user_id)
                        <div class="block_message">
                            <div class="block_message_img">
                                <img src="/storage/avatars/{{ $seller->avatar }}">
                            </div>
                            <div class="block_message_info">
                                <div class="block_message_info_top">
                                    <div class="block_message_info_top_author_left">
                                        {{ $seller->name }} {{ $seller->surname }}
                                    </div>
                                    <div class="block_message_info_top_date_right">
                                        {{ date("d.m.Y H:m:s" ,strtotime($message->created_at)) }}
                                    </div>
                                </div>
                                <div class="block_message_info_bottom">
                                    {{ $message->content }}
                                </div>
                            </div>
                        </div>
                        @if(!empty($message->photo))
                            <div class="dialog_added_photo">
                                Прикрепленное фото:
                                <a href="/storage/img/{{ $message->photo }}" target="_blank"> <img src="/storage/img/{{ $message->photo }}" alt=""> </a>
                            </div>
                        @endif
                    @endif
                @endif
            @endforeach
        @endif
        @foreach($products as $product)
            <form action="{{ route('sendMessage', [$product->id, $seller->id, $buyer->id]) }}" method="post" enctype="multipart/form-data">
                @csrf
                    <div class="dialog_form">
                        <div class="dialog_form_img">
                            {{--если я покупатель--}}
                            @if($buyer->id == Auth::user()->id)
                                <img src="/storage/avatars/{{ $buyer->avatar }}">
                            @else
                                {{--если я продавец--}}
                                <img src="/storage/avatars/{{ $seller->avatar }}">
                            @endif
                        </div>
                        <div class="dialog_form_textarea">
                            <textarea name="message" placeholder="Введите сообщение" class="{{ $errors->has('message') ? 'form__error' : '' }}"></textarea>
                            @if ($errors->has('message'))
                                <span class="invalid-feedback" role="alert">
                                    <div class="error">{{ $errors->first('message') }}</div>
                                </span>
                            @endif
                        </div>
                    </div>
                <div class="dialog_form_add_img">
                    <div>Добавить изображение:</div>
                    <input type="file" name="messagePhoto" class="{{ $errors->has('messagePhoto') ? 'form__error' : '' }}" multiple accept="image/jpeg">
                    @if ($errors->has('messagePhoto'))
                        <span class="invalid-feedback" role="alert">
                                <div class="error">{{ $errors->first('messagePhoto') }}</div>
                        </span>
                    @endif
                    <div class="btn_send_block">
                        <button type="submit" class="btn_send">Отправить</button>
                    </div>
                </div>
            </form>
        @endforeach
    </div>
@endsection
