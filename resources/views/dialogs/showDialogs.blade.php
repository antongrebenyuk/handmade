@extends('layouts.personalarea')

@section('content')
    <div class="container show__dialogs">
                @if(isset($dialogs))
                    @foreach($dialogs as $dialog)
                        @if(isset($dialog->product->name))
                            <div class="dialog_block">
                                <div class="dialog_block_img">
                                    @if($dialog->buyer)
                                        <img src="/storage/avatars/{{ $dialog->buyer->avatar }}">
                                    @elseif($dialog->seller)
                                        <img src="/storage/avatars/{{ $dialog->seller->avatar }}">
                                    @endif
                                </div>
                                <div class="dialog_block_info">
                                    <div class="dialog_block_info_top">
                                        {{ $dialog->product->name }}
                                    </div>
                                    <div class="dialog_block_info_bottom">
                                        @if(isset($dialog->messages[0]))
                                            <div class="dialog_block_info_bottom_text">
                                                <div>{{ $dialog->messages->first()->author->name }} {{ $dialog->messages->first()->author->surname }}</div>
                                            </div>
                                            <div class="dialog_block_info_bottom_text">
                                                {{ Str::limit($dialog->messages[0]->content, 20) }}
                                            </div>
                                        @else
                                            not found
                                        @endif
                                    </div>
                                </div>
                                <div class="dialog_block_goto">
                                    <a href="{{ route('dialog', [$dialog->product->id, $dialog->seller_id, $dialog->buyer_id]) }}">
                                        <img src="../storage/img/open.png">
                                    </a>
                                </div>
                            </div>
                        @endif
                    @endforeach
                @else
                    <div>У вас еще нет диалогов о покупках</div>
                @endif
    </div>

@endsection
