@inject('basket', 'App\Services\Basket')

        <!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Merriweather:400,400i,700,700i" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/style.css">
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <title>HandMade</title>
</head>
<body>
<header>
    <div class="container">
        <div class="logo">
            <a href="/">
                <span class="hand__logo">Hand</span>
                <span class="made__logo">Made.<span class="yarmarka__logo">ярмарка</span></span>
            </a>
        </div>
    </div>
</header>
<main>
    @yield('content')
</main>
<footer>
    <div class="container">
        <div class="contacts">
            <p>Наши контакты:</p>
            <p>Телефоны: +380504269752, +380934578932</p>
            <p>E-mail: handmade.yarmarka@gmail.com</p>
        </div>
        <div class="the__rights">
            <p>© 2019 г. “HandMade Ярмарка”</p>
            <p>Все права защищены.</p>
        </div>
    </div>
</footer>
<script src="js/script.js"></script>
</body>
</html>
