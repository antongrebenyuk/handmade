@inject('basket', 'App\Services\Basket')

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Merriweather:400,400i,700,700i" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <title>HandMade</title>
</head>
<body>
<header>
    <div class="container">
        <div class="logo">
            <a href="/">
                <span class="hand__logo">Hand</span>
                <span class="made__logo">Made.<span class="yarmarka__logo">ярмарка</span></span>
            </a>
        </div>
        <div class="reg__info__cart__block">
            @guest
                <a href="{{ route('login') }}" class="reg__auth__btn">Авторизация/Регистрация</a>
            @endguest
            @auth
                <div class="info__cart__btns lk__logout">
                    <a href="/user/{{ Auth::user()->id }}" class="personal__area__btn">{{ Str::limit(Auth::user()->login, 15) }}</a>
                    <a class="logout__btn" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        {{ __('Выход') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            @endauth
            <div class="info__cart__btns">
                <a href="/info" class="info__btn">О ярмарке</a>
                @if($basket->getCountItemsInOrder()===0)
                    <a href="/basket" class="cart__btn">Корзина</a>
                @else($basket->getCountItemsInOrder()>0)
                    <a href="/basket" class="cart__btn">Корзина <span class="count__products__cart">{{ $basket->getCountItemsInOrder() }}</span></a>
                @endif
            </div>
        </div>
    </div>
</header>
<main>
    <div class="container personal__area__main__page">
        <div class="personal__area__buttons__block">
            <a href="{{ route('createProduct') }}" id="{{ Route::currentRouteName() == 'createProduct' ? 'active' : ''}}">Добавить изделие</a>
            <a href="{{ route('allUserProducts') }}" id="{{ Route::currentRouteName() == 'allUserProducts' ? 'active' : ''}}">Мои товары</a>
            <a href="{{ route('mySales') }}" id="{{ Route::currentRouteName() == 'mySales' ? 'active' : ''}}">Мои продажи</a>
            <a href="{{ route('myPurchases') }}" id="{{ Route::currentRouteName() == 'myPurchases' ? 'active' : ''}}">Мои покупки</a>
            <a href="{{ route('showDialogs') }}" id="{{ Route::currentRouteName() == 'showDialogs' ? 'active' : ''}}">Мои сообщения</a>
        </div>
    </div>
    @yield('content')
</main>
<footer>
    <div class="container">
        <div class="contacts">
            <p>Наши контакты:</p>
            <p>Телефоны: +380504269752, +380934578932</p>
            <p>E-mail: handmade.yarmarka@gmail.com</p>
        </div>
        <div class="the__rights">
            <p>© 2019 г. “HandMade Ярмарка”</p>
            <p>Все права защищены.</p>
        </div>
    </div>
</footer>
<script src="js/script.js"></script>
</body>
</html>
