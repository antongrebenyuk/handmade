@extends('layouts.personalarea')

@section('content')
    <div class="container my__product__block">
        <form action="{{ route('searchProducts') }}" method="post" class="search__form" id="search__form">
            @csrf
            <input type="text" placeholder="Поиск..." name="search" class="search__input">
            <button class="clear__search__btn" type="button"><i class="fas fa-times"></i></button>
            <button type="submit"><i class="fas fa-search"></i></button>
        </form>
        <div class="my__products">
            @foreach($products as $product)
                <div class="my__product__card">
                    <img src="/storage/img/{{ $product->photo_id }}" alt="">
                    <div class="info__my__product">
                        <a href="{{ route('showProduct', $product->id) }}">
                            <div class="my__product__title">{{ $product->name }}</div>
                            <div class="my__product__price"><b>Цена:</b> {{ $product->price }} грн</div>
                            <div class="my__product__age"><b>Возраст:</b> {{ $product->product_ages_id }}</div>
                            <div class="my__product__date"><b>Дата
                                    публикации:</b> {{ date("d.m.Y" ,strtotime($product->created_at)) }}</div>
                            <div class="delete__edit__my__product">
                                <a href="{{ route('edit', $product->id) }}" class="edit__my__product"><i
                                            class="far fa-edit"></i></a>
                                <a href="{{ route('destroy', $product->id) }}" onClick="return window.confirm('Удалить товар?');"><i class="far fa-trash-alt"></i></a>
                            </div>
                        </a>

                    </div>
                </div>
            @endforeach
        </div>
        <div class="pagination__block">
            @if($products->total() > 9)
                <a href="/?page={{ $products->url(1) }}" class="first__page"><<</a>
                {{ $products->links() }}
                <a href="/?page={{ $products->lastPage() }}" class="last__page">>></a>
            @endif
        </div>
    </div>

    <script>
        // $(function () {
        //     $('.fa-trash-alt').click(function () {
        //         confirm('Удалить товар?')
        //     })
        // })
    </script>
@endsection
