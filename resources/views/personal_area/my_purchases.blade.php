@extends('layouts.personalarea')

@section('content')
    <div class="container my__product__block">
        <form action="{{ route('searchProducts') }}" method="post" class="search__form" id="search__form">
            @csrf
            <input type="text" placeholder="Поиск..." name="search" class="search__input">
            <button class="clear__search__btn" type="button"><i class="fas fa-times"></i></button>
            <button type="submit"><i class="fas fa-search"></i></button>
        </form>
        <div class="my__products">
            @foreach($items as $item)
                @if($item->product && $item->order)
                    <div class="my__product__card">
                        <img src="/storage/img/{{ $item->product->productPhoto->photo }}" alt="">
                        <div class="info__my__product">
                            <a href="{{ route('showProduct', $item->product_id) }}">
                                <div class="my__product__title">{{ $item->product->name }}</div>
                                <div class="my__product__price"><b>Цена:</b> {{ $item->product->price }}
                                    грн
                                </div>
                                <div class="my__product__age">
                                    <b>Возраст:</b> {{ $item->product->productAge->name }}</div>
                                <div class="my__product__quantity"><b>Количество:</b>{{ $item->quantity_items }}</div>
                                <div class="my__product__status"><b>Статус:</b>{{ $item->status->status }}</div>
                                <div class="my__product__date"><b>Дата
                                        публикации:</b> {{ date("d.m.Y" ,strtotime($item->product->created_at)) }}
                                </div>
                            </a>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
        <div class="pagination__block">
            {{--@if($items->product->total() > 9)--}}
            {{--<a href="/?page={{ $items->url(1) }}" class="first__page"><<</a>--}}
            {{--{{ $products->links() }}--}}
            {{--<a href="/?page={{ $items->lastPage() }}" class="last__page">>></a>--}}
            {{--@endif--}}
        </div>
    </div>
@endsection
