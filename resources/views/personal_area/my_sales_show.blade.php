@extends('layouts.app')

@section('content')
    <div class="container show__product">
        <div class="navigation">
            <a href="/">HANDMADE.ярмарка</a>
            <span>►</span>
            <a href="{{ route('mySales') }}">Мои продажи</a>
            <span>►</span>
            @foreach($items as $item)
                <a href="{{ route('category', $item->product->category_id) }}">{{ $item->product->category->name }}</a>
                <span>►</span>
                <span>{{ Str::limit($item->product->name, 30) }}</span>
            @endforeach
        </div>
        <div class="img__title">
            @foreach($items as $item)
                <div class="product__img"><img src="/storage/img/{{ $item->product->productPhoto->photo }}" alt="">
                </div>
                <div class="product__title__btns">
                    <div class="product__title">
                        <div class="title">{{ $item->product->name }}</div>
                        <div class="product__price">{{ $item->product->price }} грн.</div>
                    </div>
                    <div class="product__btns">
                        <a href="{{ route('dialog', [$item->product->id, Auth::user()->id, $item->order->user_id]) }}"
                           class="msg__to__seller__btn">Написать покупателю</a>
                    </div>
                </div>
        </div>
        @endforeach
        <div class="info__products__block">
            @foreach($items as $item)
                <ul>
                    <li><span class="info__span">Цвет:</span>{{ $item->product->color }}</li>
                    <li><span class="info__span">Количество:</span>{{ $item->quantity_items }}</li>
                    <li><span class="info__span">Возраст:</span>{{ $item->product->productAge->name }}</li>
                    <li><span class="info__span">Описание:</span>{{ $item->product->description }}</li>
                    <li><span class="info__span">Статус:</span>{{ $item->status->status }}
                        <i class="far fa-edit"></i></li>
                    <li class="status__form__hide">
                        <form action="{{ route('editStatus', [$item->order_id, $item->product_id]) }}" method="post">
                            @csrf
                            <select name="orderStatus" id="">
                                <option value="" disabled selected>Статуст заказа</option>
                                @foreach($statuses as $status)
                                    <option value="{{ $status->id }}">{{ $status->status }}</option>
                                @endforeach
                            </select>
                            <button type="submit" onClick="return window.confirm('Изменить статус товара?');">Сохранить</button>
                        </form>
                    </li>
                    <li>
                        <span class="info__span">Покупатель:</span>{{ $item->order->user->name }} {{ $item->order->user->surname }}
                    </li>
                    <li><span class="info__span">Телефон покупателя:</span>{{ $item->order->user->phone }}</li>
                    <li><span class="info__span">Дата продажи:</span>{{ date("d.m.Y" ,strtotime($item->created_at)) }}
                    </li>
                </ul>
            @endforeach
        </div>
    </div>

    <script>
        $(function () {
            $('.fa-edit').click(function () {
                $('.status__form__hide').attr('class', 'status__form__show')
            })
        })
    </script>
@endsection
