@extends('layouts.personalarea')

@section('content')

    <div class="container create__product__page">
        <div class="requared__text"><span class="text-danger">'*'</span> - обязательные поля для заполнения</div>
        <form action="{{ route('store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div>Наименование<span class="text-danger">*</span></div>
            @if($errors->has('name'))
                <div class="error">{{ $errors->first('name') }}</div>
            @endif
            <input type="text" name="name" value="{{ old('name') }}"
                   class="{{ $errors->has('name') ? 'form__error' : '' }}">
            <div>Цена(грн)<span class="text-danger">*</span></div>
            @if($errors->has('price'))
                <div class="error">{{ $errors->first('price') }}</div>
            @endif
            <input type="number" name="price" value="{{ old('price') }}"
                   class="{{ $errors->has('price') ? 'form__error' : '' }}">
            <div>Цвет<span class="text-danger">*</span></div>
            @if($errors->has('color'))
                <div class="error">{{ $errors->first('color') }}</div>
            @endif
            <input type="text" name="color" value="{{ old('color') }}"
                   class="{{ $errors->has('color') ? 'form__error' : '' }}">
            <div>Количество<span class="text-danger">*</span></div>
            @if($errors->has('quantity'))
                <div class="error">{{ $errors->first('quantity') }}</div>
            @endif
            <input type="number" min="1" name="quantity" value="{{ old('quantity') }}"
                   class="{{ $errors->has('quantity') ? 'form__error' : '' }}">
            <div>Возраст<span class="text-danger">*</span></div>
            @if($errors->has('age'))
                <div class="error">{{ $errors->first('age') }}</div>
            @endif
            <select name="age" id="" class="{{ $errors->has('age') ? 'form__error' : '' }}">
                <option value="" disabled selected>Возраст изделия</option>
                @foreach($productAges as $productAge)
                    <option value="{{ $productAge->id }}">{{ $productAge->name }}</option>
                @endforeach
            </select>
            <div>Категория<span class="text-danger">*</span></div>
            @if($errors->has('category'))
                <div class="error">{{ $errors->first('category') }}</div>
            @endif
            <select name="category" id="" class="{{ $errors->has('category') ? 'form__error' : '' }}">
                <option value="" disabled selected>Категория</option>
                @foreach($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
            <div>Описание<span class="text-danger">*</span></div>
            @if($errors->has('description'))
                <div class="error">{{ $errors->first('description') }}</div>
            @endif
            <textarea name="description"
                      class="{{ $errors->has('description') ? 'form__error' : '' }}">{{ old('description') }}</textarea>
            @if($errors->has('photo'))
                <div class="error">{{ $errors->first('photo') }}</div>
            @endif
            <div>Добавить главное изображение<span class="text-danger">*</span></div>
            <input type="file" name="photo" class="product__photo">
            @if($errors->has('photo2'))
                <div class="error">{{ $errors->first('photo2') }}</div>
            @endif
            <div>Добавить второе изображение</div>
            <input type="file" name="photo2" class="product__photo">
            @if($errors->has('photo3'))
                <div class="error">{{ $errors->first('photo3') }}</div>
            @endif
            <div>Добавить третье изображение</div>
            <input type="file" name="photo3" class="product__photo">
            <div class="clear__send__btns">
                <button type="button" class="clear__form">Очистить</button>
                <button type="submit">Сохранить</button>
            </div>
        </form>
    </div>

    <script>
        $(function () {
            $('.clear__form').click(function () {
                $('input, select, textarea').val('')
            })
        })
    </script>
@endsection
