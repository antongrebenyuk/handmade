@extends('layouts.app')

@section('content')
    @extends('layouts.personalarea')

@section('content')

    <div class="container create__product__page">
        <div class="requared__text"><span class="text-danger">'*'</span> - обязательные поля для заполнения</div>
        <form action="{{ route('update', $product->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="_method" value="PUT">
            <div>Изображение товара</div>
            <input type="file" name="productImage">
            <div>Наименование<span class="text-danger">*</span></div>
            @if($errors->has('name'))
                <div class="error">{{ $errors->first('name') }}</div>
            @endif
            <input type="text" name="name" value="{{ $product->name }}">
            <div>Цена(грн)<span class="text-danger">*</span></div>
            @if($errors->has('price'))
                <div class="error">{{ $errors->first('price') }}</div>
            @endif
            <input type="number" name="price" value="{{ $product->price }}">
            <div>Цвет<span class="text-danger">*</span></div>
            @if($errors->has('color'))
                <div class="error">{{ $errors->first('color') }}</div>
            @endif
            <input type="text" name="color" value="{{ $product->color }}">
            <div>Количество<span class="text-danger">*</span></div>
            @if($errors->has('quantity'))
                <div class="error">{{ $errors->first('quantity') }}</div>
            @endif
            <input type="number" name="quantity" value="{{ $product->quantity }}">
            <div>Возраст<span class="text-danger">*</span></div>
            @if($errors->has('age'))
                <div class="error">{{ $errors->first('age') }}</div>
            @endif
            <select name="age" id="">
                <option value="" disabled selected>Возраст изделия</option>
                @foreach($productAges as $productAge)
                    <option value="{{ $productAge->id }}">{{ $productAge->name }}</option>
                @endforeach
            </select>
            <div>Категория<span class="text-danger">*</span></div>
            @if($errors->has('category'))
                <div class="error">{{ $errors->first('category') }}</div>
            @endif
            <select name="category" id="">
                <option value="" disabled selected>Категория</option>
                @foreach($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
            <div>Описание<span class="text-danger">*</span></div>
            @if($errors->has('description'))
                <div class="error">{{ $errors->first('description') }}</div>
            @endif
            <textarea name="description">{{ $product->description }}</textarea>
            <div class="clear__send__btns">
                <button type="button" class="clear__form">Очистить</button>
                <button type="submit">Сохранить</button>
            </div>
        </form>
    </div>

    <script>
        $(function() {
            $('.clear__form').click(function(){
                $('input, select, textarea').val('')
            })
        })
    </script>

@endsection
