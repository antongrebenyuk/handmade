@inject('itemsInBasket', 'App\Services\Basket')
@extends('layouts.app')

@section('content')
    <div class="modal-overlay">
        <div class="modal">

            <a class="close-modal">
                <svg viewBox="0 0 20 20">
                    <path fill="#000000" d="M15.898,4.045c-0.271-0.272-0.713-0.272-0.986,0l-4.71,4.711L5.493,4.045c-0.272-0.272-0.714-0.272-0.986,0s-0.272,0.714,0,0.986l4.709,4.711l-4.71,4.711c-0.272,0.271-0.272,0.713,0,0.986c0.136,0.136,0.314,0.203,0.492,0.203c0.179,0,0.357-0.067,0.493-0.203l4.711-4.711l4.71,4.711c0.137,0.136,0.314,0.203,0.494,0.203c0.178,0,0.355-0.067,0.492-0.203c0.273-0.273,0.273-0.715,0-0.986l-4.711-4.711l4.711-4.711C16.172,4.759,16.172,4.317,15.898,4.045z"></path>
                </svg>
            </a><!-- close modal -->

            <div class="modal-content">
                    <div>
                        <div class="pr__name__w">
                            <a href="" class="product__name__in__window"></a>
                            <span>добавлен в корзину</span>
                        </div>
                        <div>
                            <a href="/basket" class="basket__btn">Перейти в корзину</a>
                            <button type="button" class="continue__shop__btn">Продолжить покупки</button>
                        </div>
                    </div>
            </div><!-- content -->

        </div><!-- modal -->
    </div><!-- overlay -->
    <div class="container index">
        <aside>
            <form action="{{ route('filterProducts') }}" method="post" class="filter__form">
                @csrf
                <div class="form__select__cat">
                    <select name="categoryList" id="">
                        <option value="{{ isset($categoryId) ? $categoryId : ''}}" disabled selected>{{ isset($categoryId) ? $category->name : 'Категория'}}</option>
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="filter__price__block">
                    <span class="price__text">Диапазон цен, грн.</span>
                    <input type="number" placeholder="От"  min="1" name="minPrice" value="{{ isset($minPrice) ? $minPrice : ''}}">
                    <input type="number" placeholder="До" min="1" name="maxPrice" value="{{ isset($maxPrice) ? $maxPrice : ''}}">
                    @if ($errors->has('minPrice') || $errors->has('maxPrice'))
                        <span class="invalid-feedback" role="alert">
                            <span>{{ $errors->first('minPrice') }}</span>
                            <span>{{ $errors->first('maxPrice') }}</span>
                        </span>
                    @endif
                </div>
                <div class="filter__age__block">
                    <span class="age__text">Возраст</span>
                    <div class="form__select__age">
                        <select name="ageList" id="">
                            <option value="{{ isset($ageProductId) ? $ageProductId : 'Возраст изделия'}}" disabled selected>{{ isset($ageProductId) ? $age->name : 'Возраст изделия'}}</option>
                            @foreach($productAges as $productAge)
                                <option value="{{ $productAge->id }}">{{ $productAge->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="filter__color__block">
                    <span class="age__text">Цвет</span>
                    <input type="text" name="color" value="{{ isset($color) ? $color : ''}}">
                </div>
                <div class="filter__btns__block">
                    <a href="/" class="reverse__filter">Сброс всех фильтров</a>
                    <button type="submit">Применить фильтры</button>
                </div>
            </form>
        </aside>
        <section class="search__products__list">
            <form action="{{ route('searchProducts') }}" method="post" class="search__form" id="search__form">
                @csrf
                <input type="text" placeholder="Поиск..." name="search" class="search__input" value="{{ isset($searchProductName) ? $searchProductName : '' }}">
                <button class="clear__search__btn" type="button"><i class="fas fa-times"></i></button>
                <button type="submit"><i class="fas fa-search"></i></button>
            </form>
            @if(Request::url() === 'http://grebeniuk.students.academy.aiti20.com')
            <section class="new__products__list">
                <div class="new__title">Новинки</div>
                <div class="product__cards">
                    @foreach($newProducts as $newProduct)
                        <div class="product__card">
                            <a href="{{ route('showProduct', $newProduct->id) }}" class="link__card">
                                <img src="/storage/img/{{ $newProduct->photo_id }}" alt="">
                                <span class="card__title">{{ Str::limit($newProduct->name, 20) }}</span>
                                <span class="price__card">{{ $newProduct->price }} грн.</span>
                            </a>
                            <a href="{{ route('showProduct', $newProduct->id) }}" class="view__btn">Просмотреть</a>
                            @if(Auth::user())
                                @if($newProduct->user_id !== Auth::user()->id)
                                    <form action="{{ route('addItemToOrder') }}" method="post" onsubmit="return false">
                                        @csrf
                                        <button class="add__to__cart__btn" value="{{ $newProduct->id }}">Добавить в корзину</button>
                                    </form>
                                @else
                                    <div class="my__product">Ваш товар</div>
                                @endif
                            @endif
                        </div>
                    @endforeach
                </div>
            </section>
            @endif
            <section class="all__product__list">
                <div class="all__product__title">Все изделия</div>
                <div class="product__cards">
                    @foreach($products as $product)
                        <div class="product__card">
                            <a href="{{ route('showProduct', $product->id) }}" class="link__card">
                                <img src="/storage/img/{{ $product->photo_id }}" alt="">
                                <span class="card__title">{{ Str::limit($product->name, 20) }}</span>
                                <span class="price__card">{{ $product->price }} грн.</span>
                            </a>
                            <a href="{{ route('showProduct', $product->id) }}" class="view__btn">Просмотреть</a>
                            @if(Auth::user())
                                @if($product->user_id !== Auth::user()->id)
                                    <form action="{{ route('addItemToOrder') }}" method="post" onsubmit="return false">
                                        @csrf
                                        <button href="#" class="add__to__cart__btn" value="{{ $product->id }}">Добавить в корзину</button>
                                    </form>
                                @else
                                    <div class="my__product">Ваш товар</div>
                                @endif
                            @endif
                        </div>
                    @endforeach
                        </div>
                        <div class="pagination__block">
                            @if($products->total() > 9)
                                <a href="/?page={{ $products->url(1) }}" class="first__page"><<</a>
                                {{ $products->links() }}
                                <a href="/?page={{ $products->lastPage() }}" class="last__page">>></a>
                            @endif
                        </div>
            </section>
        </section>
    </div>

    <script>
        var elements = $('.modal-overlay, .modal');

        $('.add__to__cart__btn').click(function(){
            elements.addClass('active');
        });

        $('.close-modal, .continue__shop__btn').click(function(){
            elements.removeClass('active');
        });
    </script>



    <script>
        $(function() {
            $('.clear__search__btn').click(function(){
                $('.search__input').val('')
            })
        })
    </script>

    <script>
        $(function(){
            $('.add__to__cart__btn').click(function(){
                var product_id = $(this).val();

                $.ajax({
                    url: '{{ route('addItemToOrder') }}',
                    type: 'POST',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: ({product_id: product_id}),
                    dataType: 'json',
                    success: function(count){
                        $('.count__products__cart').text(count.countProducts);//количество товаров в корзине
                        $('.product__name__in__window').text(count.item.name)//имя товара положенного в корзину
                        $('.product__name__in__window').attr('href', count.item.product_id)//ид товара полшоженного в корзину
                    },
                    error: function(){

                    }
                })

            })
        })
    </script>
@endsection
