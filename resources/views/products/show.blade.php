@extends('layouts.app')

@section('content')
    <div class="modal-overlay">
        <div class="modal">

            <a class="close-modal">
                <svg viewBox="0 0 20 20">
                    <path fill="#000000"
                          d="M15.898,4.045c-0.271-0.272-0.713-0.272-0.986,0l-4.71,4.711L5.493,4.045c-0.272-0.272-0.714-0.272-0.986,0s-0.272,0.714,0,0.986l4.709,4.711l-4.71,4.711c-0.272,0.271-0.272,0.713,0,0.986c0.136,0.136,0.314,0.203,0.492,0.203c0.179,0,0.357-0.067,0.493-0.203l4.711-4.711l4.71,4.711c0.137,0.136,0.314,0.203,0.494,0.203c0.178,0,0.355-0.067,0.492-0.203c0.273-0.273,0.273-0.715,0-0.986l-4.711-4.711l4.711-4.711C16.172,4.759,16.172,4.317,15.898,4.045z"></path>
                </svg>
            </a><!-- close modal -->
            <div class="modal-content">
                <div>
                    <div class="pr__name__w">
                        <a href="" class="product__name__in__window"></a>
                        <span>добавлен в корзину</span>
                    </div>
                    <div>
                        <a href="/basket" class="basket__btn">Перейти в корзину</a>
                        <button type="button" class="continue__shop__btn">Продолжить покупки</button>
                    </div>
                </div>
            </div><!-- content -->
        </div><!-- modal -->
    </div><!-- overlay -->
    <div class="container show__product">
        <div class="navigation">
            <a href="/">HANDMADE.ярмарка</a>
            <span>►</span>
            @foreach($products as $product)
                <a href="{{ route('category', $allProducts->category_id) }}">{{ $product->category_id }}</a>
                <span>►</span>
                <span>{{ Str::limit($product->name, 30) }}</span>
            @endforeach
        </div>
        <div class="img__title">
            @foreach($products as $product)
                <div class="product__img">
                    <a href="storage/img/{{ $product->main_photo }}" target="_blank"> <img src="storage/img/{{ $product->main_photo }}" alt="" class="main__img"> </a>
                    <div class="second__images">
                        @if(isset($product->second_photo))
                            <div class="second__image">
                                <a href="storage/img/{{ $product->second_photo }}" target="_blank"> <img src="storage/img/{{ $product->second_photo }}" alt=""> </a>
                            </div>
                        @endif
                        @if(isset($product->third_photo))
                            <div class="second__image">
                                <a href="storage/img/{{ $product->third_photo }}" target="_blank"> <img src="storage/img/{{ $product->third_photo }}" alt=""> </a>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="product__title__btns">
                    <div class="product__title">
                        <div class="title">{{ $product->name }}</div>
                        <div class="product__price">{{ $product->price }} грн.</div>
                    </div>
                    <div class="product__btns">
                        @if(Auth::user())
                            @if($product->user_id !== Auth::user()->id)
                                <form action="{{ route('addItemToOrder') }}" method="post" onsubmit="return false">
                                    @csrf
                                    <button href="#" class="add__to__cart__btn add__to__cart__btn__show"
                                            value="{{ $product->id }}">Добавить в корзину
                                    </button>
                                </form>
                                <div>
                                    <a href="{{ route('dialog', [$product->id, $product->user_id, Auth::user()->id]) }}"
                                       class="msg__to__seller__btn">Написать продавцу</a></div>
                            @else
                                <div class="my__product">
                                    <div class="delete__edit__my__product">
                                        <a href="{{ route('edit', $product->id) }}" class="edit__my__product"><i
                                                    class="far fa-edit"></i></a>
                                        <a href="{{ route('destroy', $product->id) }}" onClick="return window.confirm('Удалить товар?');"><i class="far fa-trash-alt"></i></a>
                                    </div>
                                </div>
                            @endif
                        @else
                            <div class="reg__to">Для покупки товара или связи с продавцом необходимо <a href="/login">авторизироваться</a>
                            </div>
                        @endif
                    </div>
                </div>
            @endforeach
        </div>
        <div class="info__products__block">
            @foreach($products as $product)
                <ul>
                    <li><span class="info__span">Цвет:</span>{{ $product->color }}</li>
                    <li><span class="info__span">Количество:</span>{{ $product->quantity }}</li>
                    <li><span class="info__span">Возраст:</span>{{ $product->product_ages_id }}</li>
                    <li><span class="info__span">Описание:</span>{{ $product->description }}</li>
                </ul>
            @endforeach
        </div>
    </div>

    <script>
        var elements = $('.modal-overlay, .modal');

        $('.add__to__cart__btn').click(function () {
            elements.addClass('active');
        });

        $('.close-modal, .continue__shop__btn').click(function () {
            elements.removeClass('active');
        });
    </script>

    <script>
        $(function () {
            $('.add__to__cart__btn').click(function () {
                var product_id = $(this).val();

                $.ajax({
                    url: '{{ route('addItemToOrder') }}',
                    type: 'POST',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: ({product_id: product_id}),
                    dataType: 'json',
                    success: function (count) {
                        $('.count__products__cart').text(count.countProducts);
                        $('.product__name__in__window').text(count.item.name)
                    },
                    error: function () {

                    }
                })

            })
        })
    </script>
@endsection
