@extends('layouts.app')

@section('content')

    <div class="container user__profile">
        @foreach($users as $user)
            <div class="user__profile__title">Профиль</div>
            <ul>
                <li><img src="../storage/avatars/{{ $user->avatar }}" alt="" style="width: auto"></li>
                <li><b>Имя:</b> {{$user->name}}</li>
                <li><b>Фамилия:</b> {{$user->surname}}</li>
                <li><b>Отчество:</b> {{$user->patronymic}}</li>
                <li><b>E-mail:</b> {{$user->email}}</li>
                <li><b>Дата рождения:</b> {{$user->birthday}}</li>
                <li><b>Телефон:</b> {{ $user->phone }}</li>
                <li><b>Дата регистрации:</b> {{ date("d.m.Y H:m:s" ,strtotime($user->created_at))  }}</li>
            </ul>
            <a href="/user/{{$user->id}}/edit" class="edit__profile">Редактировать</a>
        @endforeach
    </div>
@endsection
