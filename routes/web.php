<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('review', function (){
    return view('review');
});
Route::get('/info', function (){
    return view('info');
});

Route::get('/test', function (){
    return view('test');
});

Route::post('/add-item', 'OrderController@addItemToOrder')->name('addItemToOrder')->middleware('auth');
Route::get('/basket', 'OrderController@showItemsInBasket')->name('basket')->middleware('auth');
Route::get('/basket/{id}/delete', 'OrderController@destroyItemInOrder')->name('destroyItem')->middleware('auth');
Route::post('/get-sum', 'OrderController@getSum')->name('get-sum');
Route::post('/basket/register-order', 'OrderController@registerOrder')->name('registerOrder')->middleware('auth');

Route::get('/dialog/{product_id}/{seller_id}/{buyer_id}', 'DialogController@showDialog')->name('dialog')->middleware('auth');
Route::post('/dialog/{product_id}/{seller_id}/{buyer_id}', 'DialogController@createDialogAndMessage')->name('sendMessage')->middleware('auth');
Route::get('/personal-area/dialogs', 'DialogController@showDialogs')->name('showDialogs')->middleware('auth');


Route::get('/personal-area/products', 'PersonalAreaController@index')->name('allUserProducts')->middleware('auth');;
Route::get('/personal-area/create', 'ProductController@create')->name('createProduct')->middleware('auth');;
Route::get('/personal-area/products/{id}/edit', 'ProductController@edit')->name('edit')->middleware('auth');;
Route::put('/personal-area/products/{id}', 'ProductController@update')->name('update')->middleware('auth');;
Route::post('/personal-area', 'ProductController@store')->name('store')->middleware('auth');;
Route::get('/personal-area/{id}/delete', 'ProductController@destroy')->name('destroy')->middleware('auth');;
Route::get('/personal-area/my-purchases', 'PersonalAreaController@showMyPurchases')->name('myPurchases')->middleware('auth');
Route::get('/personal-area/my-sales', 'PersonalAreaController@showMySales')->name('mySales')->middleware('auth');
Route::get('/personal-area/products', 'PersonalAreaController@index')->name('allUserProducts')->middleware('auth');
Route::get('/personal-area/create', 'ProductController@create')->name('createProduct');
Route::get('/personal-area/products/{id}/edit', 'ProductController@edit')->name('edit')->middleware('auth');
Route::put('/personal-area/products/{id}', 'ProductController@update')->name('update')->middleware('auth');
Route::post('/personal-area', 'ProductController@store')->name('store')->middleware('auth');
Route::get('/personal-area/{id}/delete', 'ProductController@destroy')->name('destroy')->middleware('auth');
Route::get('/personal-area/my-sales/{id}', 'PersonalAreaController@showMySalesOneItem')->name('itemSales')->middleware('auth');
Route::post('/personal-area/my-sales/{orderId}/{productId}', 'OrderController@editItemStatus')->name('editStatus');

Route::get('/', 'ProductController@index')->name('allProducts');
Route::post('/filter-products', 'ProductController@filterProducts')->name('filterProducts');
Route::post('/search-products', 'ProductController@searchProduct')->name('searchProducts');
Route::get('/category/{id}', 'ProductController@showCategory')->name('category');
Route::get('/{id}', 'ProductController@show')->name('showProduct');






Route::get('/user/{id}', 'UserController@show')->name('showUser')->middleware('auth');
Route::get('/user/{id}/edit', 'UserController@edit')->name('editUser')->middleware('auth');
Route::put('/user/{id}', 'UserController@update')->name('updateUser')->middleware('auth');


